﻿using System;
using algorytmy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace algorytmyTest
{
    [TestClass]
    public class PointedGraphLoaderTest
    {
        [TestMethod]
        public void pointedGraphLoaderTest()
        {
            PointedGraphLoader loader = new PointedGraphLoader();
            GraphWithCoordinatesAndProfits graph = loader.LoadTestData(@"C:\Users\KrzysztofKirylowicz\Downloads\projekt_testy\test.txt");
            NearestNeighbor nearestNeighbor = new NearestNeighbor();
            nearestNeighbor.Compute(graph, 0, 7600);
        }
    }
}
