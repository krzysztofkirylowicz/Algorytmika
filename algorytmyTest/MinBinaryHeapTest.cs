﻿using algorytmy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace algorytmyTest
{
    [TestClass]
    public class MinBinaryHeapTest
    {
        [TestMethod]
        public void IsEmptyTest()
        {
            var heap = new MinBinaryHeap<long>();
            Assert.IsTrue(heap.IsEmpty());
            heap.Insert(1, 1);
            Assert.IsFalse(heap.IsEmpty());
            heap.DeleteMin();
            Assert.IsTrue(heap.IsEmpty());
        }

        [TestMethod]
        public void InsertionAscendingTest()
        {
            var priorities = new List<long>();
            var heap = new MinBinaryHeap<long>();
            for (int i = 1; i <= 31; i++)
            {
                heap.Insert(i, i);
                priorities.Add(i);
            }
            var heapPriorities = new List<long>();
            foreach (var element in heap.mItems)
            {
                heapPriorities.Add(element.Item1);
            }

            CollectionAssert.AreEqual(priorities, heapPriorities);
        }

        [TestMethod]
        public void InsertionDescendingTest()
        {
            var priorities = new List<long> { 1, 10, 2, 15, 11, 7, 3, 22, 16, 14, 12, 18, 8, 6, 4, 31, 25, 28, 17, 29, 23, 24, 13, 30, 21, 26, 9, 27, 19, 20, 5 };
            var heap = new MinBinaryHeap<long>();
            for (int i = 31; i > 0; i--)
            {
                heap.Insert(i, i);
            }

            var heapPriorities = new List<long>();
            foreach (var element in heap.mItems)
            {
                heapPriorities.Add(element.Item1);
            }

            CollectionAssert.AreEqual(priorities, heapPriorities);
        }

        [TestMethod]
        public void EditPriorityByValueTest()
        {
            var priorities = new List<int> { -1, 3, 1, 7 };
            var pointers = new Dictionary<long, int> { { 1, 2 }, { 3, 1 }, { 5, 0 }, { 7, 3 } };
            var heap = new MinBinaryHeap<long>();
            heap.Insert(1, 1);
            heap.Insert(3, 3);
            heap.Insert(5, 5);
            heap.Insert(7, 7);
            heap.EditPriorityByValue(-1, 5);
            var heapPriorities = new List<int>();
            foreach (var element in heap.mItems)
            {
                heapPriorities.Add(element.Item1);
            }

            CollectionAssert.AreEqual(priorities, heapPriorities);
            CollectionAssert.AreEqual(pointers.ToList(), heap.mPointersMap.ToList());
        }
    }
}