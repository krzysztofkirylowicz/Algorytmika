﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace algorytmy
{
    public class FarthestVertexMethod
    {
        public PathWithWeightAndProfit Compute(GraphWithCoordinatesAndProfits input, int startVertex, int dmax)
        {
            PathWithWeightAndProfit result = new PathWithWeightAndProfit();
            List<bool> visitedVertexes = new List<bool>();
            visitedVertexes.AddRange(Enumerable.Repeat(false, input.Adjacences.Count()));
            visitedVertexes[startVertex] = true;
            result.Add(startVertex, 0, 0);
            int visitedVertexesCount = 1;

            while (visitedVertexesCount != input.Adjacences.Count)
            {
                int nextVertex = FindFarthestVertexTo(result, input);
                if (result.Weight + input.Distances[result.GetLastVertex()][nextVertex] + input.Distances[nextVertex][startVertex] > dmax)
                {
                    break;
                }
                visitedVertexes[nextVertex] = true;
                visitedVertexesCount++;
                EmplaceVertex(result, input, nextVertex);
            }

            result.Add(startVertex, input.Distances[result.GetLastVertex()][startVertex], 0);

            result.Profit = result.Vertexes.Sum(v => input.Profits[v]);

            return result;
        }

        private int FindFarthestVertexTo(Path inputPath, Graph inputGraph)
        {
            int resultVertex = -1;
            int resultDistance = Int32.MinValue;
            for (int i = 0; i < inputGraph.Distances.Count; i++)
            {
                if (inputPath.Vertexes.Contains(i))
                {
                    continue;
                }

                int distanceFromPath = inputGraph.Distances[inputPath.Vertexes.First()][i];
                foreach (int pathVertex in inputPath.Vertexes)
                {
                    if (distanceFromPath < inputGraph.Distances[pathVertex][i])
                    {
                        distanceFromPath = inputGraph.Distances[pathVertex][i];
                    }
                }

                if (distanceFromPath > resultDistance)
                {
                    resultDistance = distanceFromPath;
                    resultVertex = i;
                }
            }
            return resultVertex;
        }

        private void EmplaceVertex(PathWithWeightAndProfit path, Graph graph, int vertex)
        {
            Utils utils = new Utils();
            int minWeight = int.MaxValue;
            int minWeightIndex = -1;
            if (path.Vertexes.Count == 1)
            {
                path.Vertexes.Add(vertex);
                path.Weight = path.Weight + graph.Adjacences[path.Vertexes.First()][vertex];
                return;
            }

            for (int i = 0; i < path.Vertexes.Count; i++)
            {
                List<int> newPath = new List<int>(path.Vertexes);
                newPath.Insert(i + 1, vertex);
                int newPathWeight = utils.CalculateWeight(newPath, graph);
                if (newPathWeight < minWeight)
                {
                    minWeight = newPathWeight;
                    minWeightIndex = i + 1;
                }
            }
            path.Vertexes.Insert(minWeightIndex, vertex);
            path.Weight = minWeight;
        }
    }
}