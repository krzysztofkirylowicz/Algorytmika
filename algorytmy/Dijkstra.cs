﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace algorytmy
{
    public class Dijkstra
    {
        public PathWithWeightAndProfit DijkstraAlgorithm(Graph graph, int srcVertex, int dstVertex)
        {
            var previous = Enumerable.Repeat(-1, graph.Adjacences.Count()).ToList();
            var weights = Enumerable.Repeat(Int32.MaxValue / 2, graph.Adjacences.Count()).ToList();
            var scanned = Enumerable.Repeat(false, graph.Adjacences.Count()).ToList();

            var nodes = new MinBinaryHeap<int>();

            weights[srcVertex] = 0;

            for (int i = 0; i < graph.Adjacences.Count(); i++)
            {
                int distance = i == srcVertex ? 0 : int.MaxValue / 2;
                nodes.Insert(distance, i);
            }

            while (!nodes.IsEmpty())
            {
                var smallest = nodes.GetMin();
                nodes.DeleteMin();

                scanned[smallest.Item2] = true;

                foreach (var neighbor in graph.Adjacences[smallest.Item2])
                {
                    if (!scanned[neighbor.Key])
                    {
                        int alternativeDistance = weights[smallest.Item2] + neighbor.Value;
                        if (alternativeDistance < weights[neighbor.Key])
                        {
                            weights[neighbor.Key] = alternativeDistance;
                            previous[neighbor.Key] = smallest.Item2;
                            nodes.EditPriorityByValue(alternativeDistance, neighbor.Key);
                        }
                    }
                }
            }

            var result = new List<int>();
            int index = dstVertex;
            while (index != -1)
            {
                result.Add(index+1);
                index = previous[index];
            }
            result.Reverse();

            return new PathWithWeightAndProfit(result, weights[dstVertex]);
        }

        public SortedDictionary<int, int> DijkstraAlgorithmMulti(Graph graph, int srcVertex)
        {
            var previous = Enumerable.Repeat(-1, graph.Adjacences.Count()).ToList();
            var weights = Enumerable.Repeat(Int32.MaxValue / 2, graph.Adjacences.Count()).ToList();
            var scanned = Enumerable.Repeat(false, graph.Adjacences.Count()).ToList();

            var nodes = new MinBinaryHeap<int>();

            weights[srcVertex] = 0;

            for (int i = 0; i < graph.Adjacences.Count(); i++)
            {
                int distance = i == srcVertex ? 0 : int.MaxValue / 2;
                nodes.Insert(distance, i);
            }

            while (!nodes.IsEmpty())
            {
                var smallest = nodes.GetMin();
                nodes.DeleteMin();

                scanned[smallest.Item2] = true;

                foreach (var neighbor in graph.Adjacences[smallest.Item2])
                {
                    if (!scanned[neighbor.Key])
                    {
                        int alternativeDistance = weights[smallest.Item2] + neighbor.Value;
                        if (alternativeDistance < weights[neighbor.Key])
                        {
                            weights[neighbor.Key] = alternativeDistance;
                            previous[neighbor.Key] = smallest.Item2;
                            nodes.EditPriorityByValue(alternativeDistance, neighbor.Key);
                        }
                    }
                }
            }
            var result = new SortedDictionary<int, int>();

            for (var i = 0; i< graph.Adjacences.Count;i++)
            {
                result.Add(i, weights[i]);
            }


            return result;
        }
    }
}
