﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace algorytmy
{
    public class TwoOptLocalSearch
    {
        public PathWithWeightAndProfit Compute(PathWithWeightAndProfit input1, GraphWithCoordinatesAndProfits graph)
        {
            var input = new PathWithWeightAndProfit(input1);
            Utils utils = new Utils();
            HashSet<Tuple<int, int>> applied = new HashSet<Tuple<int, int>>();
            int diff = int.MaxValue;
            int iteration = 0;
            do
            {
                diff = int.MaxValue;
                int firstVertexIndexToChange = -1;
                int secondVertexIndexToChange = -1;
                for (int i = 1; i < input.Vertexes.Count - 2; i++)
                {
                    for (int j = i + 1; j < input.Vertexes.Count - 1; j++)
                    {
                        int stepDiff = ComputeWeightDiff(i, j, input, graph);
                        if (stepDiff < diff && !applied.Contains(new Tuple<int, int>(i, j)))
                        {
                            diff = stepDiff;
                            firstVertexIndexToChange = i;
                            secondVertexIndexToChange = j;
                        }
                    }
                }
                iteration++;
                if (diff < 0 && diff > -10000)
                {
                    applied.Add(new Tuple<int, int>(firstVertexIndexToChange, secondVertexIndexToChange));
                    int tmp = input.Vertexes[firstVertexIndexToChange];
                    input.Vertexes[firstVertexIndexToChange] = input.Vertexes[secondVertexIndexToChange];
                    input.Vertexes[secondVertexIndexToChange] = tmp;
                    var newWeight = utils.CalculateWeight(input.Vertexes, graph);
                    Debug.WriteLine($"Last weight: {input.Weight}, Swapped {input.Vertexes[firstVertexIndexToChange]} and {input.Vertexes[secondVertexIndexToChange]}, new weight: {newWeight}");
                    input.Weight = newWeight;
                }
            }
            while (diff < 0);

            var profit = 0;
            foreach (var vertex in input.Vertexes)
                profit += graph.Profits[vertex];

            input.Profit = profit;

            return input;
        }

        private int ComputeWeightDiff(int firstChangeVertexIndex, int secondChangeVertexIndex, PathWithWeightAndProfit input, GraphWithCoordinatesAndProfits graph)
        {
            int removedEdgesWeight = 0;
            if (firstChangeVertexIndex + 1 != secondChangeVertexIndex)
            {
                removedEdgesWeight += graph.Distances[input.Vertexes[firstChangeVertexIndex]][input.Vertexes[firstChangeVertexIndex + 1]];
                removedEdgesWeight += graph.Distances[input.Vertexes[secondChangeVertexIndex - 1]][input.Vertexes[secondChangeVertexIndex]];
            }

            removedEdgesWeight += graph.Distances[input.Vertexes[firstChangeVertexIndex - 1]][input.Vertexes[firstChangeVertexIndex]];
            removedEdgesWeight += graph.Distances[input.Vertexes[secondChangeVertexIndex]][input.Vertexes[secondChangeVertexIndex + 1]];

            int addedEdgesWeight = 0;
            if (firstChangeVertexIndex + 1 != secondChangeVertexIndex)
            {
                addedEdgesWeight += graph.Distances[input.Vertexes[secondChangeVertexIndex]][input.Vertexes[firstChangeVertexIndex + 1]];
                addedEdgesWeight += graph.Distances[input.Vertexes[secondChangeVertexIndex - 1]][input.Vertexes[firstChangeVertexIndex]];
            }

            addedEdgesWeight += graph.Distances[input.Vertexes[firstChangeVertexIndex - 1]][input.Vertexes[secondChangeVertexIndex]];
            addedEdgesWeight += graph.Distances[input.Vertexes[firstChangeVertexIndex]][input.Vertexes[secondChangeVertexIndex + 1]];

            return addedEdgesWeight - removedEdgesWeight;

        }
    }
}
