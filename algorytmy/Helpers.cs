﻿using System.Collections.Generic;

namespace algorytmy
{
    public class Helpers
    {
        public static Dictionary<int, string> CitiesDictionary = new Dictionary<int, string>()
        {
            {1, "Bolesławiec"},
            {2, "Dzierżoniów"},
            {3, "Głogów"},
            {4, "Góra"},
            {5, "Jawor"},
            {6, "Jelenia Góra"},
            {7, "Kamienna Góra"},
            {8, "Kłodzko"},
            {9, "Legnica"},
            {10, "Lubań"},
            {11, "Lubin"},
            {12, "Lwówek Śląski"},
            {13, "Milicz"},
            {14, "Oleśnica"},
            {15, "Oława"},
            {16, "Polkowice"},
            {17, "Strzelin"},
            {18, "Środa Śląska"},
            {19, "Świdnica"},
            {20, "Trzebnica"},
            {21, "Wałbrzych"},
            {22, "Wołów"},
            {23, "Wrocław"},
            {24, "Ząbkowice Śląskie"},
            {25, "Zgorzelec"},
            {26, "Złotoryja"},
            {27, "Biała Podlaska"},
            {28, "Biłgoraj"},
            {29, "Chełm"},
            {30, "Hrubieszów"},
            {31, "Janów Lubelski"},
            {32, "Krasnystaw"},
            {33, "Kraśnik"},
            {34, "Lubartów"},
            {35, "Lublin"},
            {36, "Łęczna"},
            {37, "Łuków"},
            {38, "Opole Lubelskie"},
            {39, "Parczew"},
            {40, "Puławy"},
            {41, "Radzyń Podlaski"},
            {42, "Ryki"},
            {43, "Świdnik"},
            {44, "Tomaszów Lubelski"},
            {45, "Włodawa"},
            {46, "Zamość"},
            {47, "Gorzów Wielkopolski"},
            {48, "Krosno Odrzańskie"},
            {49, "Międzyrzecz"},
            {50, "Nowa Sól"},
            {51, "Słubice"},
            {52, "Strzelce Krajeńskie"},
            {53, "Sulęcin"},
            {54, "Świebodzin"},
            {55, "Wschowa"},
            {56, "Zielona Góra"},
            {57, "Żagań"},
            {58, "Żary"},
            {59, "Bełchatów"},
            {60, "Brzeziny"},
            {61, "Kutno"},
            {62, "Łask"},
            {63, "Łęczyca"},
            {64, "Łowicz"},
            {65, "Łódź"},
            {66, "Opoczno"},
            {67, "Pabianice"},
            {68, "Pajęczno"},
            {69, "Piotrków Trybunalski"},
            {70, "Poddębice"},
            {71, "Radomsko"},
            {72, "Rawa Mazowiecka"},
            {73, "Sieradz"},
            {74, "Skierniewice"},
            {75, "Tomaszów Mazowiecki"},
            {76, "Wieluń"},
            {77, "Wieruszów"},
            {78, "Zduńska Wola"},
            {79, "Bochnia"},
            {80, "Brzesko"},
            {81, "Chrzanów"},
            {82, "Dąbrowa Tarnowska"},
            {83, "Gorlice"},
            {84, "Kraków"},
            {85, "Limanowa"},
            {86, "Miechów"},
            {87, "Myślenice"},
            {88, "Nowy Sącz"},
            {89, "Nowy Targ"},
            {90, "Olkusz"},
            {91, "Oświęcim"},
            {92, "Proszowice"},
            {93, "Sucha Beskidzka"},
            {94, "Tarnów"},
            {95, "Zakopane"},
            {96, "Wadowice"},
            {97, "Wieliczka"},
            {98, "Białobrzegi"},
            {99, "Ciechanów"},
            {100, "Garwolin"},
            {101, "Gostynin"},
            {102, "Grodzisk Mazowiecki"},
            {103, "Grójec"},
            {104, "Kozienice"},
            {105, "Lipsko"},
            {106, "Łosice"},
            {107, "Maków Mazowiecki"},
            {108, "Mińsk Mazowiecki"},
            {109, "Mława"},
            {110, "Nowy Dwór Mazowiecki"},
            {111, "Ostrołęka"},
            {112, "Ostrów Mazowiecka"},
            {113, "Płock"},
            {114, "Płońsk"},
            {115, "Przasnysz"},
            {116, "Przysucha"},
            {117, "Pułtusk"},
            {118, "Radom"},
            {119, "Siedlce"},
            {120, "Sierpc"},
            {121, "Sochaczew"},
            {122, "Sokołów Podlaski"},
            {123, "Szydłowiec"},
            {124, "Warszawa"},
            {125, "Węgrów"},
            {126, "Wyszków"},
            {127, "Zwoleń"},
            {128, "Żuromin"},
            {129, "Żyrardów"},
            {130, "Brzeg"},
            {131, "Głubczyce"},
            {132, "Kędzierzyn-Koźle"},
            {133, "Kluczbork"},
            {134, "Krapkowice"},
            {135, "Namysłów"},
            {136, "Nysa"},
            {137, "Olesno"},
            {138, "Opole"},
            {139, "Prudnik"},
            {140, "Strzelce Opolskie"},
            {141, "Ustrzyki Dolne"},
            {142, "Brzozów"},
            {143, "Dębica"},
            {144, "Jarosław"},
            {145, "Jasło"},
            {146, "Kolbuszowa"},
            {147, "Krosno"},
            {148, "Lesko"},
            {149, "Leżajsk"},
            {150, "Lubaczów"},
            {151, "Łańcut"},
            {152, "Mielec"},
            {153, "Nisko"},
            {154, "Przemyśl"},
            {155, "Przeworsk"},
            {156, "Ropczyce"},
            {157, "Rzeszów"},
            {158, "Sanok"},
            {159, "Stalowa Wola"},
            {160, "Strzyżów"},
            {161, "Tarnobrzeg"},
            {162, "Augustów"},
            {163, "Białystok"},
            {164, "Bielsk Podlaski"},
            {165, "Grajewo"},
            {166, "Hajnówka"},
            {167, "Kolno"},
            {168, "Łomża"},
            {169, "Mońki"},
            {170, "Sejny"},
            {171, "Siemiatycze"},
            {172, "Sokółka"},
            {173, "Suwałki"},
            {174, "Wysokie Mazowieckie"},
            {175, "Zambrów"},
            {176, "Bytów"},
            {177, "Chojnice"},
            {178, "Człuchów"},
            {179, "Gdańsk"},
            {180, "Gdynia"},
            {181, "Kartuzy"},
            {182, "Kościerzyna"},
            {183, "Kwidzyn"},
            {184, "Lębork"},
            {185, "Malbork"},
            {186, "Nowy Dwór Gdański"},
            {187, "Puck"},
            {188, "Słupsk"},
            {189, "Sopot"},
            {190, "Starogard Gdański"},
            {191, "Sztum"},
            {192, "Tczew"},
            {193, "Wejherowo"},
            {194, "Bielsko-Biała"},
            {195, "Cieszyn"},
            {196, "Częstochowa"},
            {197, "Gliwice"},
            {198, "Kłobuck"},
            {199, "Lubliniec"},
            {200, "Myszków"},
            {201, "Pszczyna"},
            {202, "Racibórz"},
            {203, "Rybnik"},
            {204, "Tarnowskie Góry"},
            {205, "Zawiercie"},
            {206, "Żywiec"},
            {207, "Katowice"},
            {208, "Bytom"},
            {209, "Chorzów"},
            {210, "Dąbrowa Górnicza"},
            {211, "Jastrzębie Zdrój"},
            {212, "Jaworzno"},
            {213, "Sosnowiec"},
            {214, "Tychy"},
            {215, "Zabrze"},
            {216, "Ruda Śląska"},
            {217, "Żory"},
            {218, "Busko-Zdrój"},
            {219, "Jędrzejów"},
            {220, "Kazimierza Wielka"},
            {221, "Kielce"},
            {222, "Końskie"},
            {223, "Opatów"},
            {224, "Ostrowiec Świętokrzyski"},
            {225, "Pińczów"},
            {226, "Sandomierz"},
            {227, "Skarżysko-Kamienna"},
            {228, "Starachowice"},
            {229, "Staszów"},
            {230, "Włoszczowa"},
            {231, "Bartoszyce"},
            {232, "Braniewo"},
            {233, "Działdowo"},
            {234, "Elbląg"},
            {235, "Ełk"},
            {236, "Giżycko"},
            {237, "Gołdap"},
            {238, "Iława"},
            {239, "Kętrzyn"},
            {240, "Lidzbark Warmiński"},
            {241, "Mrągowo"},
            {242, "Nidzica"},
            {243, "Nowe Miasto Lubawskie"},
            {244, "Olecko"},
            {245, "Olsztyn"},
            {246, "Ostróda"},
            {247, "Pisz"},
            {248, "Szczytno"},
            {249, "Węgorzewo"},
            {250, "Chodzież"},
            {251, "Czarnków"},
            {252, "Gniezno"},
            {253, "Gostyń"},
            {254, "Grodzisk Wielkopolski"},
            {255, "Jarocin"},
            {256, "Kalisz"},
            {257, "Kępno"},
            {258, "Koło"},
            {259, "Konin"},
            {260, "Kościan"},
            {261, "Krotoszyn"},
            {262, "Leszno"},
            {263, "Międzychód"},
            {264, "Nowy Tomyśl"},
            {265, "Oborniki"},
            {266, "Ostrów Wielkopolski"},
            {267, "Ostrzeszów"},
            {268, "Piła"},
            {269, "Pleszew"},
            {270, "Poznań"},
            {271, "Rawicz"},
            {272, "Słupca"},
            {273, "Szamotuły"},
            {274, "Środa Wielkopolska"},
            {275, "Śrem"},
            {276, "Turek"},
            {277, "Wągrowiec"},
            {278, "Wolsztyn"},
            {279, "Września"},
            {280, "Złotów"},
            {281, "Białogard"},
            {282, "Choszczno"},
            {283, "Drawsko Pomorskie"},
            {284, "Goleniów"},
            {285, "Gryfice"},
            {286, "Gryfino"},
            {287, "Kamień Pomorski"},
            {288, "Kołobrzeg"},
            {289, "Koszalin"},
            {290, "Łobez"},
            {291, "Myślibórz"},
            {292, "Police"},
            {293, "Pyrzyce"},
            {294, "Sławno"},
            {295, "Stargard"},
            {296, "Szczecinek"},
            {297, "Świdwin"},
            {298, "Wałcz"},
            {299, "Szczecin"},
            {300, "Śwonoujście"}
        };
    }
}
