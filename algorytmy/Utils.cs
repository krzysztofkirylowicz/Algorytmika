﻿using System.Collections.Generic;
using System.Linq;

namespace algorytmy
{
    public class VertexWeightPair
    {
        public int DstVertex { get; set; }
        public int Weight { get; set; }

        public VertexWeightPair(int d, int w)
        {
            DstVertex = d;
            Weight = w;
        }
    }

    public struct Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }
    }

    public class Graph
    {
        public List<SortedDictionary<int, int>> Adjacences;
        public List<SortedDictionary<int, int>> Distances;

        public Graph(int numOfVertexes)
        {
            Adjacences = new List<SortedDictionary<int, int>>();

            for (int i = 0; i < numOfVertexes; i++)
            {
                Adjacences.Add(new SortedDictionary<int, int>());
            }
        }

        public void AddConnection(int src, int dst, int weight, bool directed = true)
        {
            Adjacences[src - 1].Add(dst - 1, weight);
            if (!directed && src != dst)
            {
                Adjacences[dst - 1].Add(src - 1, weight);
            }
        }

        public void AddConnectionWithoutChangingIndexes(int src, int dst, int weight, bool directed = true)
        {
            if (!Adjacences[src].ContainsKey(dst))
            {
                Adjacences[src].Add(dst, weight);
                if (!directed && src != dst)
                {
                    Adjacences[dst].Add(src, weight);
                }
            }
        }

        public void PrepareDistances()
        {

            Distances = new List<SortedDictionary<int, int>>();

            for (int i = 0; i < Adjacences.Count; i++)
            {
                Distances.Add(new SortedDictionary<int, int>());
            }

            var dijkstra = new Dijkstra();
            for (int i = 0; i < Adjacences.Count; i++)
            {
                Distances[i] = dijkstra.DijkstraAlgorithmMulti(this, i);
            }
        }
    }

    public class GraphWithCoordinatesAndProfits : Graph
    {
        public Dictionary<int, Point> Points;
        public Dictionary<int, int> Profits;

        public GraphWithCoordinatesAndProfits(int numOfVertexes) : base(numOfVertexes)
        {
            Points = new Dictionary<int, Point>();
            Profits = new Dictionary<int, int>();
        }

        public void AddPoint(int vertex, Point point)
        {
            Points[vertex] = point;
        }

        public void AddProfit(int vertex, int profit)
        {
            Profits[vertex] = profit;
        }
    }

    public class Path
    {
        public IList<int> Vertexes { get; set; }

        public Path()
        {
            Vertexes = new List<int>();
        }

        public Path(IList<int> vertexes)
        {
            Vertexes = vertexes;
        }

        public int GetLastVertex()
        {
            return Vertexes.Last();
        }

    }

    public class PathWithWeightAndProfit : Path
    {
        public int Weight { get; set; }
        public int Profit { get; set; }

        public PathWithWeightAndProfit() : base()
        {
            Weight = 0;
        }

        public PathWithWeightAndProfit(PathWithWeightAndProfit p)
        {
            Weight = p.Weight;
            Profit = p.Profit;
            Vertexes = p.Vertexes;
        }

        public PathWithWeightAndProfit(IList<int> vertexes, int weight) : base(vertexes)
        {
            Weight = weight;
        }

        public void Add(int vertex, int weight, int profit)
        {
            Vertexes.Add(vertex);
            this.Weight += weight;
            this.Profit += profit;
        }

        public override bool Equals(object obj)
        {
            var other = obj as PathWithWeightAndProfit;
            if (other == null)
            {
                return false;
            }

            if (other.Weight != this.Weight)
            {
                return false;
            }

            return other.Vertexes.SequenceEqual(this.Vertexes);
        }
    }

    public class Utils
    {
        public int CalculateWeight(IList<int> vertexes, Graph graph)
        {
            int result = 0;
            for (int i = 0; i < vertexes.Count - 1; i++)
            {
                result += graph.Distances[vertexes[i]][vertexes[i + 1]];
            }
            return result;
        }

        public int CalculateProfit(Path path, GraphWithCoordinatesAndProfits graph)
        {
            int result = 0;
            foreach (int vertex in path.Vertexes)
            {
                result += graph.Profits[vertex];
            }
            return result;
        }
    }
}
