﻿namespace algorytmy
{
    public class InsertLocalSearch
    {
        public PathWithWeightAndProfit Compute(PathWithWeightAndProfit input1, GraphWithCoordinatesAndProfits graph, int dmax)
        {
            var input = new PathWithWeightAndProfit(input1);
            int weightDiff = int.MinValue;
            do
            {
                weightDiff = int.MinValue;
                var insertAfterIndex = -1;
                var vertexToInsert = -1;
                var value = double.MinValue;
                for (int i = 0; i < graph.Adjacences.Count; i++)
                {
                    if (!input.Vertexes.Contains(i))
                    {
                        for (int j = 0; j < input.Vertexes.Count - 2; j++)
                        {
                            int tmpDiff = ComputeWeightDiff(input, graph, j, i);
                            if (tmpDiff != 0 && graph.Profits[i] / tmpDiff > value && input.Weight + tmpDiff <= dmax)
                            {
                                value = graph.Profits[i] / tmpDiff;
                                weightDiff = tmpDiff;
                                insertAfterIndex = j;
                                vertexToInsert = i;
                            }
                        }
                    }
                }

                if (weightDiff > 0 && weightDiff < 100000)
                {
                    input.Weight += weightDiff;
                    input.Vertexes.Insert(input.Vertexes.IndexOf(input.Vertexes[insertAfterIndex]) + 1, vertexToInsert);
                }
            } while (weightDiff > 0 && weightDiff < 100000);

            var profit = 0;
            foreach (var vertex in input.Vertexes)
                profit += graph.Profits[vertex];

            input.Profit = profit;

            return input;
        }

        private int ComputeWeightDiff(PathWithWeightAndProfit input, GraphWithCoordinatesAndProfits graph, int insertAfter, int vertexToInsert)
        {
            return graph.Distances[input.Vertexes[insertAfter]][vertexToInsert]
                + graph.Distances[vertexToInsert][input.Vertexes[insertAfter + 1]]
                - graph.Distances[input.Vertexes[insertAfter]][input.Vertexes[insertAfter + 1]];
        }
    }
}
