﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace algorytmy
{
    public class NearestNeighbor
    {
        public PathWithWeightAndProfit Compute(GraphWithCoordinatesAndProfits input, int startVertex, int dmax)
        {
            PathWithWeightAndProfit result = new PathWithWeightAndProfit();
            List<bool> visitedVertexes = new List<bool>();
            visitedVertexes.AddRange(Enumerable.Repeat(false, input.Adjacences.Count()));
            visitedVertexes[startVertex] = true;
            result.Add(startVertex, 0, 0);
            int visitedVertexesCount = 1;

            Random random = new Random();


            bool shouldEnd = false;
            while (visitedVertexesCount != input.Adjacences.Count && !shouldEnd)
            {
                var resultLastVertex = result.GetLastVertex();
                var neighbors = input.Distances[resultLastVertex].Where(n => !visitedVertexes[n.Key]);

                int randomNumber = random.Next(0, 100);
                if (randomNumber < 20)
                {
                    var sortedNeighbors = neighbors.Where(x=> x.Value> 0).OrderByDescending(kvp =>input.Profits[kvp.Key] / kvp.Value);
                    foreach (KeyValuePair<int, int> pair in sortedNeighbors)
                    {
                        if (pair.Key != resultLastVertex && !visitedVertexes[pair.Key])
                        {
                            if (result.Weight + pair.Value + input.Distances[pair.Key][startVertex] > dmax)
                            {
                                shouldEnd = true;
                                break;
                            }
                            result.Add(pair.Key, pair.Value, input.Profits[pair.Key]);
                            visitedVertexes[pair.Key] = true;
                            visitedVertexesCount++;
                            break;
                        }
                    }
                }
                else
                {
                    if(neighbors == null || !neighbors.Any())
                        shouldEnd = true;

                    var randomNotVisitedNeightbor = neighbors.ElementAt(random.Next(0, neighbors.Count()));
                    if (result.Weight + randomNotVisitedNeightbor.Value + input.Distances[randomNotVisitedNeightbor.Key][startVertex] > dmax)
                    {
                        //shouldEnd = true;
                    }
                    else
                    {
                        result.Add(randomNotVisitedNeightbor.Key, randomNotVisitedNeightbor.Value, input.Profits[randomNotVisitedNeightbor.Key]);
                        visitedVertexes[randomNotVisitedNeightbor.Key] = true;
                        visitedVertexesCount++;
                    }
                }
            }

            result.Add(startVertex, input.Distances[result.GetLastVertex()][startVertex], 0);

            return result;
        }
    }
}