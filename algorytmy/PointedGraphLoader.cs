﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace algorytmy
{
    public class PointedGraphLoader
    {
        private int Distance(Point a, Point b)
        {
            return (int)Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        public GraphWithCoordinatesAndProfits LoadTestData(string path)
        {
            var lines = File.ReadLines(path);
            var firstLine = lines.FirstOrDefault();

            var result = new GraphWithCoordinatesAndProfits(int.Parse(firstLine));

            var points = new List<Point>();
            var profits = new List<int>();

            foreach (var line in lines.Skip(1))
            {
                var parts = line.Split(' ');

                var x = double.Parse(parts[0], NumberStyles.Number | NumberStyles.AllowExponent, CultureInfo.InvariantCulture);
                var y = double.Parse(parts[1], NumberStyles.Number | NumberStyles.AllowExponent, CultureInfo.InvariantCulture);

                var profit = int.Parse(parts[2]);
                points.Add(new Point(x, y));
                profits.Add(profit);
            }

            for (int i = 0; i < points.Count; i++)
            {
                result.AddPoint(i, points[i]);
                result.AddProfit(i, profits[i]);
            }

            for (int i = 0; i < points.Count; i++)
            {
                for (int j = points.Count - 1; j >= i; j--)
                {
                    result.AddConnectionWithoutChangingIndexes(i, j, Distance(points[i], points[j]), false);
                }
            }
            result.PrepareDistances();

            return result;
        }

        public GraphWithCoordinatesAndProfits Load(string path)
        {
            var lines = File.ReadLines(path).ToList();
            var firstLine = lines.FirstOrDefault();
            var firstLineParts = firstLine.Split(' ');
            var result = new GraphWithCoordinatesAndProfits(int.Parse(firstLineParts[0]));

            var points = new List<Point>();
            var profits = new List<int>();

            foreach (var line in lines.GetRange(1, 300))
            {
                var parts = line.Split(' ');

                var x = double.Parse(parts[2], NumberStyles.Number | NumberStyles.AllowExponent, CultureInfo.InvariantCulture);
                var y = double.Parse(parts[3], NumberStyles.Number | NumberStyles.AllowExponent, CultureInfo.InvariantCulture);

                var profit = int.Parse(parts[1]);
                points.Add(new Point(x, y));
                profits.Add(profit);
            }

            foreach (var line in lines.Skip(301))
            {
                var parts = line.Split(' ');
                var i = int.Parse(parts[0], NumberStyles.Number | NumberStyles.AllowExponent, CultureInfo.InvariantCulture) - 1;
                var j = int.Parse(parts[1], NumberStyles.Number | NumberStyles.AllowExponent, CultureInfo.InvariantCulture) - 1;
                result.AddConnectionWithoutChangingIndexes(i, j, int.Parse(parts[2], NumberStyles.Number | NumberStyles.AllowExponent, CultureInfo.InvariantCulture), false);
            }

            for (int i = 0; i < points.Count; i++)
            {
                result.AddPoint(i, points[i]);
                result.AddProfit(i, profits[i]);
            }

            result.PrepareDistances();

            return result;
        }
    }
}