﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("algorytmyTest")]

namespace algorytmy
{
    public class MinBinaryHeap<ObjectT> where ObjectT : IComparable<ObjectT>
    {
        internal List<Tuple<int, ObjectT>> mItems;
        internal IDictionary<ObjectT, int> mPointersMap;

        public MinBinaryHeap()
        {
            mItems = new List<Tuple<int, ObjectT>>();
            mPointersMap = new Dictionary<ObjectT, int>();
        }

        public void Insert(int priority, ObjectT obj)
        {
            mItems.Add(new Tuple<int, ObjectT>(priority, obj));
            mPointersMap.Add(obj, mItems.Count() - 1);
            BubbleUp(mItems.Count() - 1);
        }

        public Tuple<int, ObjectT> GetMin()
        {
            return mItems[0];
        }

        public void DeleteMin()
        {
            if (mItems.Count() == 0)
            {
                return;
            }

            mPointersMap.Remove(mItems[0].Item2);
            mItems[0] = mItems.Last();
            mItems.RemoveAt(mItems.Count() - 1);

            BubbleDown(0);
            RegeneratePointersMap();
        }

        public bool IsEmpty()
        {
            return mItems.Count() == 0;
        }

        public void EditPriorityByValue(int priority, ObjectT obj)
        {
            int itemIndex = mPointersMap[obj];
            int prevPriority = mItems[itemIndex].Item1;
            mItems[itemIndex] = new Tuple<int, ObjectT>(priority, mItems[itemIndex].Item2);

            if (prevPriority < priority)
            {
                BubbleDown(itemIndex);
            }
            else if (prevPriority > priority)
            {
                BubbleUp(itemIndex);
            }
            RegeneratePointersMap();
        }

        private bool CompareItems(Tuple<int, ObjectT> obj1, Tuple<int, ObjectT> obj2)
        {
            if (obj1.Item1 > obj2.Item1)
            {
                return true;
            } else if (obj1.Item1 == obj2.Item1)
            {
                return obj1.Item2.CompareTo(obj2.Item2) > 0;
            }
            return false;
            //return obj1.Item1 > obj2.Item1 || obj1.Item2.CompareTo(obj2.Item2) > 0;
        }

        private void BubbleDown(int index)
        {
            int length = mItems.Count();
            int leftChildIndex = 2 * index + 1;
            int rightChildIndex = 2 * index + 2;

            if (leftChildIndex >= length) return;  // index is a leaf

            int minIndex = index;

            if (CompareItems(mItems[index], mItems[leftChildIndex]))
            {
                minIndex = leftChildIndex;
            }

            if ((rightChildIndex < length) &&
                CompareItems(mItems[minIndex], mItems[rightChildIndex]))
            {
                minIndex = rightChildIndex;
            }

            if (minIndex != index)
            {
                // need to swap
                Tuple<int, ObjectT> temp = new Tuple<int, ObjectT>(mItems[index].Item1, mItems[index].Item2);
                mPointersMap[temp.Item2] = minIndex;
                mItems[index] = new Tuple<int, ObjectT>(mItems[minIndex].Item1, mItems[minIndex].Item2);
                mPointersMap[mItems[minIndex].Item2] = index;
                mItems[minIndex] = temp;

                BubbleDown(minIndex);
            }
        }

        private void BubbleUp(int index)
        {
            if (index == 0) return;

            int parentIndex = (index - 1) / 2;

            if (CompareItems(mItems[parentIndex], mItems[index]))
            {
                Tuple<int, ObjectT> temp = new Tuple<int, ObjectT>(mItems[parentIndex].Item1, mItems[parentIndex].Item2);
                mPointersMap[temp.Item2] = index;
                mItems[parentIndex] = new Tuple<int, ObjectT>(mItems[index].Item1, mItems[index].Item2);
                mPointersMap[mItems[index].Item2] = parentIndex;
                mItems[index] = temp;
                BubbleUp(parentIndex);
            }
        }

        private void RegeneratePointersMap()
        {
            for(int i = 0; i < mItems.Count(); i++)
            {
                mPointersMap[mItems[i].Item2] = i;
            }
        }
    }
}