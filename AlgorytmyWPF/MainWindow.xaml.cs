﻿using algorytmy;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace AlgorytmyWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private String polandPointsFilePath;
        private GraphWithCoordinatesAndProfits polandGraph;
        private String testPointsFilePath;
        private GraphWithCoordinatesAndProfits testGraph;

        public MainWindow()
        {
            InitializeComponent();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            openFileDialog.Title = "Wybierz plik danych testowych";
            if (openFileDialog.ShowDialog() == true)
            {
                testPointsFilePath = openFileDialog.FileName;
                new Task(() =>
                {
                    PointedGraphLoader loader = new PointedGraphLoader();
                    testGraph = loader.LoadTestData(testPointsFilePath);
                    if (testDataLoaded != null)
                    {
                        Dispatcher.Invoke(new Action(() =>
                        {
                            testDataLoaded.Foreground = Brushes.Green;
                            testDataLoaded.Content = "Dane testowe załadowane";
                        }));
                    }
                }).Start();
            }

            openFileDialog.Title = "Wybierz plik danych dla Polski";
            if (openFileDialog.ShowDialog() == true)
            {
                polandPointsFilePath = openFileDialog.FileName;
                new Task(() =>
                {
                    PointedGraphLoader loader = new PointedGraphLoader();
                    polandGraph = loader.Load(polandPointsFilePath);
                    if (polandDataLoaded != null)
                    {
                        Dispatcher.Invoke(new Action(() =>
                        {
                            polandDataLoaded.Foreground = Brushes.Green;
                            polandDataLoaded.Content = "Dane Polski załadowane";
                        }));
                    }
                }).Start();
            }

        }

        private void RenderBrowser(object sender, RoutedEventArgs e)
        {
            if (File.Exists($"{AppDomain.CurrentDomain.BaseDirectory.Replace("bin\\Debug\\", string.Empty)}Misc\\map.html"))
            {

                StreamReader objReader = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory.Replace("bin\\Debug\\", string.Empty)}Misc\\map.html");
                string line = "";
                line = objReader.ReadToEnd();
                objReader.Close();
                StreamWriter page = File.CreateText($"{AppDomain.CurrentDomain.BaseDirectory.Replace("bin\\Debug\\", string.Empty)}Misc\\map2.html");
                page.Write(line);
                page.Close();
                Uri uri2 = new Uri($"{AppDomain.CurrentDomain.BaseDirectory.Replace("bin\\Debug\\", string.Empty)}Misc\\map2.html");
                webBrowser.Navigate(uri2);
            }

            Uri uri = new Uri(System.IO.Path.Combine($"{AppDomain.CurrentDomain.BaseDirectory.Replace("bin\\Debug\\", string.Empty)}Misc\\map.html"));
            resultLabel.Content = string.Empty;

            webBrowser.Navigate(uri);
        }

        private void RenderPolandDataMap(object sender, RoutedEventArgs e)
        {
            if (polandGraph == null)
            {
                System.Windows.MessageBox.Show("Dane Polski nie zostały jeszcze załadowane!");
                return;
            }
            NearestNeighbor nearestNeighbor = new NearestNeighbor();
            FarthestVertexMethod farthestVertexMethod = new FarthestVertexMethod();
            TwoOptLocalSearch twoOptLocalSearch = new TwoOptLocalSearch();
            InsertLocalSearch insertLocalSearch = new InsertLocalSearch();

            PathWithWeightAndProfit result1;
            if (first.IsChecked ?? false)
                result1 = nearestNeighbor.Compute(polandGraph, 0, int.Parse(textBox.Text));
            else
                result1 = farthestVertexMethod.Compute(polandGraph, 0, int.Parse(textBox.Text));

            resultLabel.Content = $"{result1.Weight} / {result1.Vertexes.Count} / {result1.Profit}             ";

            while (true)
            {
                var result = twoOptLocalSearch.Compute(result1, polandGraph);
                if (result.Weight < result1.Weight)
                    result1 = result;

                var b = result1.Vertexes.Count;
                var res = insertLocalSearch.Compute(result1, polandGraph, int.Parse(textBox.Text));
                if (res.Vertexes.Count == b)
                    break;
                else
                    result1 = res;
            }
            webBrowser.Visibility = System.Windows.Visibility.Visible;
            //var result1 = nearestNeighbor.Compute(graph, 0, 7600);
            resultLabel.Content += $"{result1.Weight} / {result1.Vertexes.Count} / {result1.Vertexes.Sum(v=>polandGraph.Profits[v])}";


            var value = JsonConvert.SerializeObject(result1.Vertexes.Select(x => new
            {
                lat = polandGraph.Points[x].Y,
                lng = polandGraph.Points[x].X
            }));

            string path = polandPointsFilePath + ".path";

            var outt = string.Empty;
            foreach (var vertex in result1.Vertexes)
            {
                outt += Helpers.CitiesDictionary[vertex + 1] + " ";
            }
            File.WriteAllText(path, outt);

            webBrowser.InvokeScript("addMarker", new Object[] {
                    value
                 });
        }

        private void RenderTestDataMap(object sender, RoutedEventArgs e)
        {
            graphics.Children.Clear();

            if (testGraph == null)
            {
                System.Windows.MessageBox.Show("Dane testowe nie zostały jeszcze załadowane!");
                return;
            }
            NearestNeighbor nearestNeighbor = new NearestNeighbor();
            FarthestVertexMethod farthestVertexMethod = new FarthestVertexMethod();
            TwoOptLocalSearch twoOptLocalSearch = new TwoOptLocalSearch();
            InsertLocalSearch insertLocalSearch = new InsertLocalSearch();

            PathWithWeightAndProfit result1;
            if (first.IsChecked ?? false)
                result1 = nearestNeighbor.Compute(testGraph, 0, int.Parse(textBox.Text));
            else
                result1 = farthestVertexMethod.Compute(testGraph, 0, int.Parse(textBox.Text));

            resultLabel.Content = $"{result1.Weight} / {result1.Vertexes.Count} / {result1.Profit}             ";

            while (true)
            {

                var result = twoOptLocalSearch.Compute(result1, testGraph);
                if (result.Weight < result1.Weight)
                    result1 = result;

                var b = result1.Vertexes.Count;
                var res = insertLocalSearch.Compute(result1, testGraph, int.Parse(textBox.Text));
                if (res.Vertexes.Count == b)
                    break;
                else
                    result1 = res;
            }

            foreach (var vertex in testGraph.Points)
            {
                Ellipse ellipse = new Ellipse
                {
                    Width = 5,
                    Height = 5,
                    Fill = Brushes.Blue
                };

                Canvas.SetLeft(ellipse, (vertex.Value.X - 5) / 2);
                Canvas.SetTop(ellipse, (vertex.Value.Y - 5) / 2);

                graphics.Children.Add(ellipse);
            }

            for (var i = 0; i <= result1.Vertexes.Count - 2; i++)
            {
                var start = testGraph.Points[result1.Vertexes[i]];
                var end = testGraph.Points[result1.Vertexes[i + 1]];

                var line = new Line
                {
                    X1 = start.X / 2,
                    Y1 = start.Y / 2,
                    X2 = end.X / 2,
                    Y2 = end.Y / 2,
                    Stroke = Brushes.Red
                };

                graphics.Children.Add(line);
            }


            webBrowser.Visibility = System.Windows.Visibility.Hidden;
            resultLabel.Content += $"{result1.Weight} / {result1.Vertexes.Count} / {result1.Vertexes.Sum(v => testGraph.Profits[v])}";

            string path = testPointsFilePath + ".path";
            File.WriteAllText(path, string.Join(" ", result1.Vertexes.Select(x => x + 1)));
        }
    }
}
